function addCompany(company) {
  document.body.innerHTML = `
    <div class="container">
      <div class="row">
        <div class="col compName">${company.name}</div>
        <div class="cur"><b>maxSize:</b><div class="col-md-auto">${company.maxSize}</div></div>
        <div class="cur"><b>curSize:</b><div class="col col-lg-2">${company.curSize + 1}</div></div>
      </div>
    </div>
    <table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Lastname</th>
        <th scope="col">Role</th>
        <th scope="col">ID</th>
        <th></th>
      </tr>
    </thead>
    <tbody class="user"></tbody>
    </table>
    
  `;
  document.querySelector('tbody').addEventListener('click', (e) => {
    if (e.target.tagName !== "SPAN") {
      return;
    }
  
    const pressButton = e.target;
    const deleteNode = pressButton.closest("tr");
    const userID = deleteNode.querySelector(".userID").textContent;
    admin.deleteUser(userID);
  });
}

function addUser(user) {
  const tr = document.createElement('tr');
  tr.setAttribute('id', user.id)
  const tbody = document.querySelector('tbody');
  tr.innerHTML = `
    <td class="number">${tbody.childElementCount + 1}</td>
    <td>${user.name}</td>
    <td>${user.lastname}</td>
    <td>${user.role}</td>
    <td class="userID">${user.id}</td>
    <td style='display: flex; justify-content: center;'>
      <button type="button" class="close" aria-label="Close">
        <span aria-hidden="true" style='color: #aaa'>&times;</span>
      </button>
    </td>
  `;
  document.querySelector('tbody').appendChild(tr);
  const currSize = document.querySelector('.col-lg-2');
  currSize.textContent = ++currSize.textContent;
  
}

function deleteCompanyUser(id) {
    const deleteNode = document.getElementById(id);
    deleteNode.remove();
    const curSize = document.querySelector(".col-lg-2");
    document.querySelectorAll('.number').forEach((i, index) => i.textContent = index + 1);
    curSize.textContent = --curSize.textContent;
}

function addNotify(message) {

  const notify = `
    <div class="toast-body">
      ${message}
    </div>`;

  document.querySelector('table').insertAdjacentHTML('afterend', notify);
  setTimeout(function() {
    document.querySelector('.toast-body').remove();
  }, 4000);
}