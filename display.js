// (
  function main() {
  window.company = new Company('Yojji', 5);
  addCompany(company);
  company.registerUserCreateCallback(user => {addUser(user); addNotify("User was added")});
  company.registerNoSpaceNotifyer(() => {addNotify(); addNotify("The company is full")});
  company.registerUserUpdateCallback((id) => {deleteCompanyUser(id); addNotify("User: " + id + " was deleted")});
  const admin = Company.createSuperAdmin(company);
  window.admin = admin;
  admin.createUser('Karina', 'Oleynik');
  admin.createUser('Alexandr', 'Semenenko');
  admin.createUser('Olga', 'Shekhovtsova');
}
// )()