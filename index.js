(function () {
  const privatPassword = Symbol('password');
  const usersArray = Symbol('users')
  const registerUserCreateCallback = [];
  const registerUserUpdateCallback = [];
  const registerNoSpaceNotifyer = [];
  
  class Company {
    constructor (name, maxSize) {
      this.name = name;
      this.maxSize = maxSize;
      this[usersArray] = [];
    }

    static createSuperAdmin(company) {

      if(company[usersArray][0]) {
        return console.warn("The admin exists already");
      }
      
      const admin = new User('admin', 'admin', true);
      admin.company = company;
      admin[privatPassword] = prompt("Enter your password: ", '');
      company[usersArray].push(admin);

      return admin;
    } 
    
    getUser(id) {
      return this[usersArray].find(el => el.id === id);
    }

    get curSize() {
      return this[usersArray].length;
    }

    registerUserCreateCallback (cb) {
      registerUserCreateCallback.push(cb);
    }

    registerUserUpdateCallback (cb) {
      registerUserUpdateCallback.push(cb);
    }

    registerNoSpaceNotifyer (cb) {
      registerNoSpaceNotifyer.push(cb);
    }

  }

  class User {
    #name;
    #lastname;
    #role;
    constructor (name, lastname, isAdmin) {
      this.#name = name;
      this.#lastname = lastname;
      this.id = Math.floor(Math.random() * 1000000000);
      this.#role = 'user';

      if (isAdmin) {
        const secretToken = Symbol('token')
        this[secretToken] = 'secret token';
        this.#role = 'admin';
        this.users = company[usersArray];
        this.createUser = function (name, lastName) {
          
          if (this.users.length+1 > this.company.maxSize) {
            return console.warn("Company is full");
          }

          var password = prompt("Enter password: ", '');
    
          if (password !== this.users[0][privatPassword]) {
            return console.warn("You don't have access. Your password is wrong!");
          }        
    
          if (this[secretToken] !== 'secret token') {
            return console.warn("Your token is not correct!");
          }

          const user = new User(name, lastName);
          this.users.push(user);
          registerUserCreateCallback.forEach(el => el(user));

          if (this.users.length == this.company.maxSize) {
            registerNoSpaceNotifyer.forEach(el => el(user));
          }
          
          return user;
        };

        this.deleteUser = function (id) {
          const password = prompt("Enter password: ", '');
    
          if (password !== this.users[0][privatPassword]) {
            return console.warn("You don't have access. Your password is wrong!");
          }
          
          const deletedUserID = this.users.find(el => el.id == id);
          const index = this.users.indexOf(deletedUserID);
          this.users.splice(index, 1);
          registerUserUpdateCallback.forEach(el => el(id));
          
          return;
        };
      } 
    }

    get name () {
      return this.#name;
    }

    set name (newName) {
      this.#name = newName;
      registerUserUpdateCallback.forEach(el => el(this));
    }

    get lastname () {
      return this.#lastname;
    }

    set lastname (newLastname) {
      this.#lastname = newLastname;
      registerUserUpdateCallback.forEach(el => el(this));
    }

    get role () {
      return this.#role;
    }

    set role (newRole) {
      this.#role = newRole;
      registerUserUpdateCallback.forEach(el => el(this));
    }
  };
  
  window.Company = Company;
})()